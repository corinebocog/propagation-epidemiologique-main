

import pygame
from random import random, randint
from math import exp
from utils.population import Population
import pandas as pd


pygame.font.init()



for i in range(100):

    n = randint(20, 1000)  # Effectifs de départ
    infected_percentage = 0.02  # Nombre d'individus infectes initialement
    CONFINEMENT_LENGTH = randint(0, 70)  # Taille du carré de confinement.
    INFECTION_DIST = 20  # Distance a partir de laquelle une personne infectée en infecte une autre

    WIN_W = 1300  # Largeur de la fenetre
    WIN_H = 600  # Hauteur de la fenetre
    velocity = 19.5  # Distance parcourue par un individu en un cycle
    MEDIAN_AGE = randint(15, 65)  # Age median de la population etudiée

    LAVAGE_MAIN = randint(0, 1)
    # TAUX_MASQUE = 0
    TAUX_MASQUE = round(random(), 2)

    # Risque (proba de mort) calculée pour une population d'age donné
    alpha = 50
    beta = 1
    gamma = 0.3

    ### NE PAS SUPPRIMER CE CONTEXTE
    # INFECTION_RISK = (1/6) * (exp(-alpha/MEDIAN_AGE) + exp(-LAVAGE_MAIN/beta)*(1 - exp((TAUX_MASQUE-1)/gamma)))
    INFECTION_RISK = (1/6) * (exp(-alpha/MEDIAN_AGE) + 1 - exp((LAVAGE_MAIN-1)/beta)*(1 - exp((-TAUX_MASQUE)/gamma)))

    print(f"{INFECTION_RISK = }")

    CRITICAL_RISK = MEDIAN_AGE / 100
    print(f"{CRITICAL_RISK = }")
    INFECTION_DURATION = 800  # Durée d'infection en cycles
    STAT_FONT = pygame.font.SysFont("comicsans", 35)  # Mise en place de la police

    win = pygame.display.set_mode((WIN_W, WIN_H))  # Création de la fenetre
    pygame.display.set_caption(
        "Simulation - Propagation épidémiologique")  # Affichage du titre
    win.fill((255, 255, 255))  # Couleur blanche pour la fenetre

    for j in range(20):
        print("SImulation i = ", i, "j = ", j)
        
        population = Population(n, infected_percentage, velocity)
        params = {
            'INFECTION_RISK'    : INFECTION_RISK,
            'INFECTION_DIST'    : INFECTION_DIST,
            'INFECTION_DURATION': INFECTION_DURATION,
            'CONFINEMENT_LENGTH': CONFINEMENT_LENGTH,
            'CRITICAL_RISK'     : CRITICAL_RISK,
        }
        population.set_params(**params)
        population.set_win(win)
        population.populate()


        tick_counter = 0  # Date de depart (en cycles)
        # Création d'un regulateur de la boucle pour avoir 60 iterations par seconde
        clock = pygame.time.Clock()
        time_since_end = 0  # Pendule pour avoir un peu de temps apres la fin de la simulation

        population.draw()


        # pygame.display.update()


        game_running = True

        while game_running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_running = False
                    break
            
            win.fill((255, 255, 255))

            population.update()

            if len(population.asymptomatic) == 0:
                time_since_end += 1
            if time_since_end >= 180:
                game_running = False
            
            # pygame.display.update()

            clock.tick(60)
            tick_counter += 1

            if tick_counter%100 == 0:
                pass
        
        row_to_add = {
            'n°': [i], 'confinement': [CONFINEMENT_LENGTH], 'age': [MEDIAN_AGE],
            'effectif': [n], 'masque': [TAUX_MASQUE],
            'lavage': [LAVAGE_MAIN], 'mortalite': [population.counter.dead / n] }

        df = pd.DataFrame(row_to_add)
        df.to_csv("C:/Users/BELONG'S TELECOMS/Desktop/EEIA/Projet/propagation-epidemiologique-main/data/test.csv", mode='a', index=False, header=False)
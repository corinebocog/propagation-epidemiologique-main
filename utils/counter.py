

class PersonCounter:

    def __init__(self, **kwargs):
        self._attr = {**kwargs}


    def increment(self, key, inc=1):
        if key in self._attr:
            self._attr[key] += inc
    

    def get(self, key, default=None):
        return self._attr.get(key, default)

    
    def add(self, key, value=0):
        self._attr[key] = value


    def __getattr__(self, key):
        if key in self._attr:
            return self._attr[key]
        else:
            raise AttributeError(f"counter does not have attribute '{key}'")
    

    def __getitem__(self, key):
        if key in self._attr:
            return self._attr[key]
        else:
            raise AttributeError(f"counter does not have attribute '{key}'")
    

    def __repr__(self):
        return str(self._attr)

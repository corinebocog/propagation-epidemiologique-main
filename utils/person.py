import pygame
from random import random
from math import sqrt

# NOTE: différents états possibles
# --------------------------------

# ----------------------------------- + -------- +
#               State                 | Int Repr |
# ----------------------------------- | -------- |
# - sain purement                     |     0    |
# - infecté:                          |          |
# 	* asymptomatique                  |     1    |
# 	* hospitalisé                     |     2    |
# - sain immunisé après infection     |     3    |
# - critique                          |     4    |
# - mort                              |     5    |
# ------------------------------------+----------+



BLUE   = (0, 0, 128)
GREEN  = (0, 255, 64)
GREEN2 = (150, 200, 0, 0.3)
GREY   = (128, 128, 128)
ORANGE = (255, 128, 64)
RED    = (255, 0, 0)
BLACK  = (0, 0, 0)


class Person:
	"""This is a model to represent a person of our simulation"""

	# Differents colors for each state
	colors = [BLUE, GREEN, ORANGE, GREY, RED, BLACK]
	
	# Integer representation for each state
	HEALTHY      = 0
	ASYMPTOMATIC = 1
	HOSPITALIZED = 2
	IMMUNED      = 3
	CRITICAL     = 4
	DEAD         = 5


	def __init__(self, x, y, xvel, yvel, state=0, population=None):
		"""Initialisation of the person

		Args:
			x (float): x-pos on the GUI
			y (float): y-pos on the GUI
			xvel (float): ux representing the velocity
			yvel (float): vy representing the velocity in y-dir
			state (int, optional): int representation of the person state.
				Defaults to 0.
			population (population.Population): population the person belong to.
		"""
		self.x = x
		self.y = y
		self.spx = x
		self.spy = y
		self.XVEL = xvel
		self.YVEL = yvel
		self.TICKS_SICK = 0  # Depuis combien de cycles la personne est malade
		self.state = state   # État de la personne (6 possibilités listées plus haut: voir NOTE)
		self.population = population

	
	def get_color(self):
		return self.colors[self.state]
	

	def euclidian_distance(self, person):
		"""Compute the euclidian distance between two people of the population

		Args:
			person (person.Person): the other person

		Returns:
			float: euclidian distance between 'self' and 'person'
		"""
		dx = self.x - person.x
		dy = self.y - person.y
		return sqrt(dx**2 + dy**2)
	

	def remove_from_infected(self):
		""" Remove the person from either asymptomatic or hospitalized array """
		if self.state == self.ASYMPTOMATIC:
			self.population.asymptomatic.remove(self)
		else:
			self.population.in_hosp.remove(self)
	

	def draw(self, win):
		""" This will draw a person on the GUI

		Args:
			win (pygame.display): simulation window
		"""
		center = round(self.x), round(self.y)
		INFECTION_DIST = self.population.params['INFECTION_DIST']
		pygame.draw.circle(win, self.get_color(), center, 10)
		if self.state == self.ASYMPTOMATIC:
			# Si la personne est infectée, et contagieuse,
			# on dessine un cercle vert qui délimite la distance de contagiosité
			radius = round(1 + (((self.TICKS_SICK%60) / 59) * (INFECTION_DIST-1)))
			pygame.draw.circle(win, GREEN2, center, radius, 1)


	def move(self, win):
		""" Utils to move a person on the GUI

		Args:
			win (pygame.display): simulation window
		"""

		WIN_W, WIN_H = win.get_size()
		CONFINEMENT_LENGTH = self.population.params['CONFINEMENT_LENGTH']
		# Si on dépasse les bords du carré de confinement, on rebondit.
		# Pour rebondir, si on tape à gauche ou à droite, on oppose
		# la vitesse sur l'axe des x, et sinon, si on tape en haut ou en bas,
		# on oppose la vitesse sur l'axe des y
		if abs(self.x - self.spx) >= CONFINEMENT_LENGTH:
			self.XVEL = -self.XVEL
		if abs(self.y - self.spy) >= CONFINEMENT_LENGTH:
			self.YVEL = -self.YVEL
		# Si on atteint les bords de la fenêtre on rebondit
		if self.x < 50 or self.x > WIN_W-20:
			self.XVEL = -self.XVEL
		if self.y < 50 or self.y > WIN_H-20:
			self.YVEL = -self.YVEL

		# Une fois qu'on a verifié que l'on n'est pas sorti des limites,
		# on ajoute la distance parcourue en un cycle à la position
		self.x += self.XVEL
		self.y += self.YVEL
	

	def met_infected(self):
		"""Check if the person met an infected person

		Returns:
			bool: True is we met an infected person, else False.
		"""
		INFECTION_DIST = self.population.params['INFECTION_DIST']
		for person in self.population.asymptomatic:
			dist = self.euclidian_distance(person)
			if dist <= INFECTION_DIST:
				return True
		return False


	def _healthy_logic(self):
		"""Logic 1: the person is healthy
		
		Notes:
			- First, we check if the person was closed (in term of distance)
			to an infected person
			- Then, we randomly draw a number to model the infection risk
			- Finally, in 15% of the cases, the person has symptoms
		"""
		if self.met_infected():
			INFECTION_RISK = self.population.params['INFECTION_RISK']
			will_be_ill = random() <= INFECTION_RISK
			if will_be_ill:
				if random() <= 0.15:
					self.state = self.HOSPITALIZED
					self.population.in_hosp.add(self)
				else:
					self.state = self.ASYMPTOMATIC
					self.population.asymptomatic.add(self)
				# decrement the number of healthy people
				self.population.increment("healthy", inc=-1)
	

	def _critical_logic(self):
		"""Logic 2: the person is in a critical state
		
		Notes:
			Here, the person will die after a certain amout of time

			--------------------------------------------------------------
			WE CAN MAKE A MODEL WHERE A PERSON CAN HEAL FROM THE INFECTION
			AND THEN BE IMMUNED (with a small probability though) !
		"""
		INFECTION_DURATION = self.population.params['INFECTION_DURATION']
		self.TICKS_SICK += 1
		if self.TICKS_SICK >= INFECTION_DURATION:
			self.state = self.DEAD
			self.XVEL = self.YVEL = 0
			self.population.critical.remove(self)
			self.population.increment("dead")


	def _infected_logic(self):
		"""Logic 3: the person is infected but not in critical case

		Notes:
			2 possible scenarios:
				- the person become 'immuned' after a given amount of time
				- the person become a 'critical case' and will die after
				a given amount of time
		"""
		
		INFECTION_DURATION = self.population.params['INFECTION_DURATION']
		self.TICKS_SICK += 1
		# scenario 1
		if self.TICKS_SICK >= INFECTION_DURATION:
			self.remove_from_infected()
			self.state = self.IMMUNED
			self.population.increment("immuned")
		# scenario 2
		elif self.state == self.HOSPITALIZED:
			CRITICAL_RISK = self.population.params['CRITICAL_RISK']
			# rd = random()
			# print(rd)
			now_critical = random() <= CRITICAL_RISK
			if now_critical:
				self.remove_from_infected()
				self.state = self.CRITICAL
				self.population.critical.add(self)

		
	
	def exec_infection_rules(self, ):
		""" This will implement the rules of the simulator """

		# Behaviour when the person is HEALTHY
		if self.state == self.HEALTHY:
			self._healthy_logic()
		
		# Behaviour when the person is in a critical state
		elif self.state == self.CRITICAL:
			self._critical_logic()
		
		# Behaviour when we are just infected
		elif self.state not in {self.IMMUNED, self.DEAD}:
			self._infected_logic()

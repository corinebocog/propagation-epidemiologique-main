import pygame
from math import exp
from utils.population import Population


class Simulateur:

    def __init__(
        self, nb_pop=100,
        infected_percentage=0.02,
        confinement_length=1000,
        infection_dist=20,
        infection_duration=800,
        win_size=(1300, 600),
        velocity=1.5,
        median_age=30,
        hand_wash=1,
        mask_rate=0,
        ):

        self.n = nb_pop
        self.infected_percentage = infected_percentage
        self.confinement_length = confinement_length
        self.infection_dist = infection_dist
        self.infection_duration = infection_duration
        self.win_size = win_size
        self.velocity = velocity
        self.median_age = median_age
        self.hand_wash = hand_wash
        self.mask_rate = mask_rate
        
        alpha = 50
        beta = 1
        gamma = 0.3
        infection_risk = (1/6) * (exp(-alpha/median_age) + 1 - exp((hand_wash-1)/beta)*(1 - exp((-mask_rate)/gamma)))
        critical_risk = median_age / 100
        params = {
            'INFECTION_RISK'    : infection_risk,
            'INFECTION_DIST'    : infection_dist,
            'INFECTION_DURATION': infection_duration,
            'CONFINEMENT_LENGTH': confinement_length,
            'CRITICAL_RISK'     : critical_risk,
        }
        
        
        # TODO
        # pygame.font.SysFont("comicsans", 35)
        # win = pygame.display.set_mode(win_size)
        # pygame.display.set_caption(
        #     "Simulation - Propagation épidémiologique")
        # win.fill((255, 255, 255))

        # self.population = Population(nb_pop, )
    
    def play(self):
        pass

    def launch(self):
        pass


    def launch_average(self, param, repeat=20):
        pass

    def simulate(self, repeat, show=False, debug=False):
        pass
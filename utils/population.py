from utils.counter import PersonCounter
from .person import Person
from random import randint, random


class Population:
	"""Class représentant la population de la simulation."""
	
	def __init__(self, n=0, infected_percentage=0.02, velocity=1.5):
		self.n = n
		self.velocity = velocity
		# Number of infected people at the beginning
		self.nb_infected = infected_percentage * n
		self.win = None      		# GUI Window
		self.people = []     		# stock people of the population
		self.asymptomatic = set()   # asymptomatic people
		self.in_hosp = set()        # hospitalized people
		self.critical = set()       # critical people
		self.dead = set()           # dead people

		self.params = {}

		# initialization du compteur
		self.init_counter()
		# # Création de la population de départ
		# self.populate()
	

	def set_win(self, win):
		self.win = win

	def set_params(self, **kwargs):
		for key, value in kwargs.items():
			self.params[key] = value

	def init_counter(self):
		counter_attr = {
			'immuned': 0,
			'healthy': 0,
			'dead': 0,
		}
		self.counter = PersonCounter(**counter_attr)


	def increment(self, key, inc=1):
		self.counter.increment(key, inc)

	def hospitals_are_full(self):
		pass


	def populate(self):
		"""Crée la population de départ en se servant des paramètres initiaux. """
		for _ in range(self.n):
			# Création d'une nouvelle personne
			new_person = self.new_person()
			self.people.append(new_person)
			# Rajout dans la liste des personnes asymptomatiques
			if new_person.state:
				self.asymptomatic.add(new_person)
		self.increment("healthy", inc=self.n - len(self.asymptomatic))
	


	def new_person(self):
		"""Crée une nouvelle personne

		Returns:
			person (person.Person): Nouvelle personne créée en fixant les paramètres initiaux
		"""

		# Définition aléatoire de la direction et du sens de l'individu
		xvel = randint(0, self.velocity*100*2) / 100 - self.velocity
		yvel = (self.velocity**2 - xvel**2)**0.5
		if randint(0, 1):
			yvel *= -1
		
		# Définition aléatoire de la position de départ
		WIN_W, WIN_H = self.win.get_size()
		xpos = randint(70, WIN_W-30)
		ypos = randint(70, WIN_H-30)
		# État de l'individu (0 ou 1)
		state = int(len(self) < self.nb_infected)
		
		return Person(xpos, ypos, xvel, yvel, state, population=self)
	

	def update(self):
		for person in self.people:
			person.move(self.win)
			person.exec_infection_rules()
			person.draw(self.win)

	def draw(self):
		for person in self.people:
			person.draw(self.win)
	
	def __len__(self):
		return len(self.people)


	def __repr__(self):
		count = ', '.join([f"{key}={value}" for key, value in self.counter._attr.items()])
		count += f", asymp={len(self.asymptomatic)}, hosp={len(self.in_hosp)}, crit={len(self.critical)}"

		return f"<Population {count}>"

	

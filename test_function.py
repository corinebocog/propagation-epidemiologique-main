
from logging import critical
import numpy.random as rnd


# print(rnd.normal(0,1.5,(1,10)))


print(rnd.poisson(10, (1, 1))[0][0])

    def move:
        if self.color == self.HOSCOLOR:
            # Deplacer dans la zone des hospitalisés


def calcul_taux_chance(min=0, max=100):
    return randint(min, max)  # Tirage aléatoire.


def check_dead(self):
    self.TICKS_SICK += 1  # Incrementation du nombre de cycles de maladie

    if self.TICKS_SICK >= DURATION:
        # On suppose que si on est en etat critique de mort, on meurt au bout de la
        # Duree de la maladie
        self.cont = False  # On est plus contaminé
        self.crit = False  # ON est plus critique
        self.dead = True  # ON est mort
        self.color = self.DEADCOLOR  # COuleur mort est noir
        self.XVEL = 0  # ON ne bouge plus
        self.YVEL = 0  # ...
        # self.IMMUNE = True  # On est immunisé

        if self in contamines:
            # On est plus dans la liste des contamines.
            contamines.remove(self)

        if self in contagieux:
            # On est plus dans la liste des contagieux.
            contagieux.remove(self)

        if self in critical:
            critical.remove(self)  # On est plus dans la liste des critiques.

        global dead
        dead += 1
        # Incrementation du nombre de morts

    else:
        self.color = self.DANGERCOLOR  # Couleur critique est rouge
        self.crit = True  # on est en etat critique
        self.dead = False
        self.cont = True  # ON est toujours contaminés
        critical.append(self)


def check_inf(self):
    # si le personne est malade, presente les symptômes de maladie et que les hopitaux sont débordés
    if self.cont == True && self.asymp == 0 && (len(inhosp) < BED_CAP):
        val = calcul_taux_chance()  # Tirage aléatoire.

        if val >= 50:  # Si val est supérieur à 50 la personne est hospitalisé.
            self.color = self.HOSCOLOR  # Couleur hospitalisation
            self.cont = True  # ON est contaminés
            inhosp.append(self)  # On occupe une place a l'hopital
            # contagieux.append(self)  # On occupe une place a l'hopital

            # Le seuil de traitement d'un individu.
            seuil_individu = calcul_taux_chance()

            # Si le seuil de traitement d'un malade est supérieur ou égal au seuil normal de traitement.
            if ((seuil_individu)/100) >= SEUIL_TRAITEMENT:
                self.cont = False  # On est plus malade
                self.color = self.IMMINECOLOR  # Couleur de l'immunisé

                # déclaration de la variable global immune_counter.
                global immune_counter

				immune_counter += 1  # Incrementation du compteur de personnes immunisées

                # on le retire de la liste des hospitalisés.
                inhosp.remove(self)

                if self in contamines:
    					# SI on est dans la liste des malades contaminés, on se retire
					contamines.remove(self)

                if self not in contagieux:
                    # : A Revoir. Nous devons vérifier si la personne hospitalisé et immunisé pourrais être contagieux ou pas. #contagieux.remove(self)  # On libere une place a l'hopital
                    # contagieux.append(self)

            else:
                # On suppose la personne n'atteind le seuil normal de traitement donc passe à l'état critique.

                # Vérifier si la personne est toujours à l'état critique sinon il meurt.
                check_dead(self)

                # Revoir dans quelle mesure une personne en état critique pourrais guérir.
                
        else:
            check_dead()

    # si le personne est malade, presente les symptômes de maladie et que les hopitaux sont débordés
    if self.cont == True && self.asymp == 0 && (len(inhosp) >= BED_CAP):
        self.TICKS_SICK += 1  # Incrementation du nombre de cycles de maladie

        if self.TICKS_SICK >= DURATION:
            # On suppose que si on est en etat critique de mort, on meurt au bout de la
			# Duree de la maladie
			self.cont = False  # On est plus contaminé
			self.crit = False  # ON est plus critique
			self.dead = True  # ON est mort
			self.color = self.DEADCOLOR  # COuleur mort est noir
			self.XVEL = 0  # ON ne bouge plus
			self.YVEL = 0  # ...
			# self.IMMUNE = True  # On est immunisé
			critical.remove(self)  # On est plus dans la liste des critiques
			global dead
			dead += 1
			# Incrementation du nombre de morts
        else:
            self.color = self.DANGERCOLOR  # Couleur critique est rouge
            self.crit = True
            self.dead = False
			self.cont = True  # On est donc contamines
			critical.append(self)

    # si le personne est malade, mais ne presente pas les symptômes de maladie
    if self.cont == True && self.asymp == 1:
        self.TICKS_SICK += 1  # Incrementation du nombre de cycles de maladie

        if self.TICKS_SICK >= DURATION:
            self.cont = False  # On est plus malade
			self.IMMUNE = True  # On est immunisés
            self.color = self.IMMUNECOLOR
            self.dead = False
			global immune_counter
			# Incrementation du compteur de personnes immunisées
			immune_counter += 1

            if self in contamines:
    			# SI on est dans la liste des malades non-hospitalises, on se retire
				contamines.remove(self)
				# contagieux.remove(self) ############ A revoir.

        else:
            if randint(0, 100) <= 40:
				self.color = self.INFECTCOLOR  # Couleur hospitalisation
                self.cont = True 
                self.crit = False
                self.dead = False
                contagieux.append(self)
            else:
                
                check_dead()
                
    to_add = False
	for case in contamines:
		# Pour chaque personne contagieuses, on calcul la distance a nous
		dist = get_dist_bet(self.x, self.y, case.x, case.y)
		if dist <= CONT_RAD:
			# Si la personne est trop proche, on devient infecté
			to_add = True           
            
    if to_add:
    				# La personne attrape la maladie
				nb = randint(0, 10000)
				# Tirage aléatoire
				if len(inhosp) >= BED_CAP:
					# Si les hopitaux sont debordés
					if nb >= 1500:
						# 15% deviennent critique et meurent
						self.color = self.DANGERCOLOR  # On devient critique
						self.crit = True
						self.cont = True  # On est donc contamines
						critical.append(self)
						contagieux.append(self) 
					else:
						# Sinon, on est just contaminé
						self.cont = True
						self.color = self.INFECTCOLOR
						contamines.append(self)
						contagieux.append(self) 

				else:
					# SI les hopitaux de sont pas debordés
					if nb <= POP_RISK*100:
						# Si la personne est dans un etat critique (depend du risque de la population)
						self.color = self.DANGERCOLOR  # Meme chose que plua haut pour les critiques
						self.crit = True
						self.cont = True
						critical.append(self)
						contagieux.append(self) 
					elif nb <= 1500:
						# Dans envrions 15%, on est hospitalisé
						self.color = self.HOSCOLOR  # Couleur hospitalisation
						self.cont = True  # ON est contaminés
						inhosp.append(self)  # On occupe une place a l'hopital
						# contagieux.append(self)  # On occupe une place a l'hopital
      
					else:
						# Sinon, on est juste infecté
						self.cont = True
						self.color = self.INFECTCOLOR
						contamines.append(self)
						contagieux.append(self) 

				global sucept_counter
				sucept_counter -= 1
				# Si on rentre en contact avec la maladie, il y a une personne
				# de moins qui est susceptible de l'attraper
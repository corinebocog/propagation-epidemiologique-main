import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def get_cmap(n, name="hsv"):
    return plt.cm.get_cmap(name, n)


col_name = ['n°', 'confinement', 'age', 'effectif', 'masque', 'lavage', 'mortalite']
data = pd.read_csv('./test.csv', delimiter=',', names=col_name)

# colors = get_cmap(50)
# fig, ax = plt.subplots()
# grouped = data.groupby('n°')
# for key, group in grouped:
#     x = key + np.arange(1, len(group)+1)
#     color = [colors(key)] * len(group)
#     # print(color)
#     ax.scatter(x, group['mortalite'], c=color)
# data.plot.scatter(x="n°", y="mortalite")

x = np.linspace(0, 1, 1000)
beta = 0.3
y = 1 - np.exp((x - 1)/beta)
plt.plot(x, y)
plt.plot(x, np.exp(-x/0.25))
plt.show()